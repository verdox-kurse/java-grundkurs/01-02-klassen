package de.verdox.grundkurs;

import java.util.Scanner;

public class SimpleJava {

    public static Scanner scanner = new Scanner(System.in);

    public static void writeInLine(int x){
        System.out.print(""+x);
    }

    public static void write (int x){
        System.out.println(""+x);
    }

    public static void writeInLine(String msg){
        System.out.print(msg);
    }

    public static void write (String msg){
        System.out.println(msg);
    }

    public static int readInt(){
        String input = scanner.nextLine();
        try{
            return Integer.parseInt(input);
        }
        catch(NumberFormatException e){
            System.out.println("Bitte gebe eine ganze Zahl ein!");
            readInt();
        }
        return 0;
    }

    public static String read(){
        return scanner.nextLine();
    }

    public static String read(String msg){
        System.out.println(msg);
        return scanner.nextLine();
    }

    public static int readInt(String msg){
        System.out.println(msg);
        String input = scanner.nextLine();
        try{
            return Integer.parseInt(input);
        }
        catch(NumberFormatException e){
            System.out.println("Bitte gebe eine ganze Zahl ein!");
            readInt(msg);
        }
        return 0;
    }
}
